import java.util.Random;
public class Methods{

public static String adjective(){//gets a word for the adjective of the sentence
  String adjective = " ";
  int randomInt = (int)(Math.random()*10);//random number to pick the word in the switch code
  switch (randomInt){
    case 0: adjective = "agressive";
      break;
    case 1: adjective = "messy";
      break;
    case 2: adjective = "brave";
      break;
    case 3: adjective = "stinky";
      break;
    case 4: adjective = "lazy";
      break;
    case 5: adjective = "quick";
      break;
    case 6: adjective = "smart";
      break;
    case 7: adjective = "sneaky";
      break;
    case 8: adjective = "small";
      break;
    case 9: adjective = "happy";
      break;
  }
  return adjective;
}
public static String subject(){//picks the subject
  String subject = " ";
  int randomInt = (int)(Math.random()*10);//random number to pick the word in the switch code
  switch (randomInt){
    case 0: subject = "child";
      break;
    case 1: subject = "mouse";
      break;
    case 2: subject = "hawk";
      break;
    case 3: subject = "lion";
      break;
    case 4: subject = "animal";
      break;
    case 5: subject = "dog";
      break;
    case 6: subject = "woman";
      break;
    case 7: subject = "man";
      break;
    case 8: subject = "pet";
      break;
    case 9: subject = "bug";
      break;
  }
  return subject;
}
public static String pastTense(){//Picks the past tense word for the sentence
  int randomInt = (int)(Math.random()*10);////random number to pick the word in the switch code
  String pastTense = " ";
  switch (randomInt){
    case 0: pastTense = "ran";
      break;
    case 1: pastTense = "kicked";
      break;
    case 2: pastTense = "survived";
      break;
    case 3: pastTense = "beat";
      break;
    case 4: pastTense = "hit";
      break;
    case 5: pastTense = "body slammed";
      break;
    case 6: pastTense = "tackeled";
      break;
    case 7: pastTense = "reported";
      break;
    case 8: pastTense = "trapped";
      break;
    case 9: pastTense = "killed";
      break;
  }
  return pastTense;
}
public static String object(){//what the random opbect of the sentence is
  String object = " ";
  int randomInt = (int)(Math.random()*10);//random number to pick the word in the switch code
  switch (randomInt){
    case 0: object = "football";
      break;
    case 1: object = "house";
      break;
    case 2: object = "pants";
      break;
    case 3: object = "balloon";
      break;
    case 4: object = "oven";
      break;
    case 5: object = "shampoo";
      break;
    case 6: object = "trash";
      break;
    case 7: object = "pumpkin";
      break;
    case 8: object = "tea";
      break;
    case 9: object = "wii";
      break;
  }
  return object;
}
public static void main(String[] args){//call the lines for the three different sentences
  conclusion(middle(thesis()));
}

public static String thesis(){//prints the thesis 
  String primSub = subject();
  System.out.println("The "+adjective() + " " + primSub + " " + pastTense() + " the "+ adjective()+" "+ object() +".");
  return primSub;
}

public static String middle(String primSub){//prints the middle sentence
    System.out.println("This "+primSub+" "+pastTense()+" "+object()+"s at the "+adjective()+" "+subject()+" using "+object()+"s.");
    return primSub;
}

public static void conclusion(String primSub){//prints the conclusion sentence
  System.out.println("That "+primSub+" "+pastTense()+" its "+object()+"!");
}
}