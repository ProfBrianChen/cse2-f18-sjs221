import java.util.Scanner;
import java.util.Random;

public class Hw05{
  public static void main(String args[]){
		Random randGen = new Random();//set up random number generator
    Scanner myScanner = new Scanner(System.in);//set up scanner
		int[] cards = new int[5];//arrays to set up the 5 cards picked
		int[] probs = new int[4];//array to set up the 4 outputs for printing 
		float[] probPrint = new float[4];//initializing the 4 values for print statement
		int[] occurences = new int[13];//counts how many occurences of the card value there are
		int loops;
		int card;
			
		//Asks for loops
		do{
			System.out.println("How many hands do you want to generate?");//ask how many hands it should generate 
		  loops = myScanner.nextInt();
		}while(loops < 0);//sets up loop to run for each hand requested
		
		//do for x loops
		for(int x = 0; x<loops;x++){//run till all hands are checked
		//generates cards
		for(int i = 0; i< 5;i++){//check for each card that is in each hand
			do{
				card = randGen.nextInt(52)+1;//gives a value to one card in hand 
			}while(card == cards[0] || card == cards[1] || card == cards[2] || card == cards[3] || card == cards[4]);//while all the cards equal each other
			cards[i] = card;	
			occurences[card%13] ++;//show occurence for each value of card by using remander of 13
		}
		boolean presentPair = false;// initalizing to show how a present pair is not true
		
		for(int i = 0;i<13;i++){
			if(occurences[i]==4){//count occurence for 4 of a kind
				probs[0]++;//and to the value 
			}
			if(occurences[i]==3){//count occurence for 3 of a kind
				probs[1]++;
			}
			if(occurences[i]==2&&presentPair){//see if there is pair with a pair already in the hand
				probs[2]++;
				probs[3]--;
			}
			if(occurences[i]==2){//see if there is a pair without another pair in the hand
				probs[3]++;
				presentPair = true;
			}
			occurences[i] = 0;//makes the occurences go back to 0
		}	
		}
		
		for(int i = 0;i<4;i++){

			probPrint[i] = (float)probs[i]/loops;//gives probability of each type of hand
		}

		System.out.println("The number of loops: "+loops);//print statements for probability
		System.out.format("The probability of Four-of-a-kind: %1.3f\n", probPrint[0]);
		System.out.printf("The probability of Three-of-a-kind: %1.3f\n", probPrint[1]);
		System.out.printf("The probability of Two-pair %1.3f\n", probPrint[2]);
		System.out.printf("The probability of One-pair: %1.3f\n", probPrint[3]);
	}
}