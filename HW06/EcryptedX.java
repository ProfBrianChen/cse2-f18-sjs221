//Samantha Sagi
//homework 6
//10/22/18
import java.util.Scanner;
public class EcryptedX{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Please enter number of rows");//ask for number of rows
    int dim= myScanner.nextInt();
for(int i = 0;i<dim;i++){//for loop for number of rows
      for(int j = 0;j<dim;j++){//nested for loops for number of columns 
       if(i==j||i+j==dim-1)//puts the blanks in the correct spots 
          System.out.print(" ");
        else
          System.out.print("*");
      }
      System.out.println("");//switches lines
    }
  }
}