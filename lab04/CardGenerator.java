//Samantha Sagi
//9.11.18
//Card Generator
// Pick a random number out of 52 and figure out which card matches the number
import java.util.Random;
public class CardGenerator{
  public static void main(String args[]){
    Random randGen = new Random();
    int randCard = randGen.nextInt(52) +1;//enters a random number
if(randCard>=1 && randCard<=13){//section for club suit
  if(randCard==1){//ask if the random number matches 1
    System.out.println("you picked the Aces of Clubs");//prints the card is an Aces of clubs 
  }
  if(randCard==2){
    System.out.println("you picked the 2 of Clubs");
  }
  if(randCard==3){
    System.out.println("you picked the 3 of Clubs");
  }
  if(randCard==4){
    System.out.println("you picked the 4 of Clubs");
  }
  if(randCard==5){
    System.out.println("you picked the 5 of Clubs");
  }
  if(randCard==6){
    System.out.println("you picked the 6 of Clubs");
  }
  if(randCard==7){
    System.out.println("you picked the 7 of Clubs");
  }
  if(randCard==8){
    System.out.println("you picked the 8 of Clubs");
  }
  if(randCard==9){
    System.out.println("you picked the 9 of Clubs");
  }
  if(randCard==10){
    System.out.println("you picked the 10 of Clubs");
  }
  if(randCard==11){
    System.out.println("you picked the jack of Clubs");
  }
  if(randCard==12){
    System.out.println("you picked the queen of Clubs");
  }
  if(randCard==13){
    System.out.println("you picked the King of Clubs");
  }
}
if(randCard>=14 && randCard<=26){//seperates the heart suits
  if(randCard==14){//ask if the card number matches 14
    System.out.println("you picked the Aces of hearts");//prints the aces of hearts 
  }
  if(randCard==15){
    System.out.println("you picked the 2 of hearts");
  }
  if(randCard==16){
    System.out.println("you picked the 3 of hearts");
  }
  if(randCard==17){
    System.out.println("you picked the 4 of hearts");
  }
  if(randCard==18){
    System.out.println("you picked the 5 of hearts");
  }
  if(randCard==19){
    System.out.println("you picked the 6 of hearts");
  }
  if(randCard==20){
    System.out.println("you picked the 7 of hearts");
  }
  if(randCard==21){
    System.out.println("you picked the 8 of hearts");
  }
  if(randCard==22){
    System.out.println("you picked the 9 of hearts");
  }
  if(randCard==23){
    System.out.println("you picked the 10 of hearts");
  }
  if(randCard==24){
    System.out.println("you picked the jack of hearts");
  }
  if(randCard==25){
    System.out.println("you picked the queen of hearts");
  }
  if(randCard==26){
    System.out.println("you picked the King of hearts");
  }
}
if(randCard>=27 && randCard<=39){//seperate the suit of spades
  if(randCard==27{//ask is the card number matches 27
    System.out.println("you picked the Aces of spades");//prints the card of aces of spades
  }
  if(randCard==28){
    System.out.println("you picked the 2 of spades");
  }
  if(randCard==29){
    System.out.println("you picked the 3 of spades");
  }
  if(randCard==30){
    System.out.println("you picked the 4 of spades");
  }
  if(randCard==31){
    System.out.println("you picked the 5 of spades");
  }
  if(randCard==32){
    System.out.println("you picked the 6 of spades");
  }
  if(randCard==33){
    System.out.println("you picked the 7 of spades");
  }
  if(randCard==34){
    System.out.println("you picked the 8 of spades");
  }
  if(randCard==35){
    System.out.println("you picked the 9 of spades");
  }
  if(randCard==36){
    System.out.println("you picked the 10 of spades");
  }
  if(randCard==37){
    System.out.println("you picked the jack of spades");
  }
  if(randCard==38){
    System.out.println("you picked the queen of spades");
  }
  if(randCard==39){
    System.out.println("you picked the King of spades");
  }
}
if(randCard>=40 && randCard<=53){//seperates the suit of diamonds
  if(randCard==40){//ask if the card number matches 40
    System.out.println("you picked the Aces of diamonds");//prints aces of diamonds
  }
  if(randCard==41){
    System.out.println("you picked the 2 of diamonds");
  }
  if(randCard==42){
    System.out.println("you picked the 3 of diamonds");
  }
  if(randCard==43){
    System.out.println("you picked the 4 of diamonds");
  }
  if(randCard==44){
    System.out.println("you picked the 5 of diamonds");
  }
  if(randCard==45){
    System.out.println("you picked the 6 of diamonds");
  }
  if(randCard==46){
    System.out.println("you picked the 7 of diamonds");
  }
  if(randCard==47){
    System.out.println("you picked the 8 of diamonds");
  }
  if(randCard==48){
    System.out.println("you picked the 9 of diamonds");
  }
  if(randCard==49){
    System.out.println("you picked the 10 of diamonds");
  }
  if(randCard==50){
    System.out.println("you picked the jack of diamonds");
  }
  if(randCard==51){
    System.out.println("you picked the queen of diamonds");
  }
  if(randCard==52){
    System.out.println("you picked the King of diamonds");
  }
}
  }
}