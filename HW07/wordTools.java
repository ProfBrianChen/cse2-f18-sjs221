import java.util.Scanner;
public class wordTools{
  public static void main(String[] args){
    Scanner in = new Scanner(System.in);
    String phrase = "";
    
   String text = sampleText();//get string text from smaple text method
   String mint;
   do{System.out.println("Text: "+text);//print text and give value to mint and run through if statements the first time before checking while loop
     mint = printMenu();
      if(mint.equals("c")){//check to see if the user wants this condition from menu
        System.out.println("Number of non-whitespace characters: "+getNumOfNonWSCharacters(text));
      }
      if(mint.equals("w")){//check to see if the user wants this condition from menu
        System.out.println("Number of words: "+getNumOfWords(text));
      }
      if(mint.equals("f")){//check to see if the user wants this condition from menu
        do{//ask for word or phrase first and run until while statement is false
        System.out.println("Enter a word or phrase to be found");
        phrase = in.nextLine();
        }while(phrase.length()<1);
        System.out.println("\""+phrase+"\" instances: "+findText(phrase, text));//print the satement after found and counted
      }
      if(mint.equals("r")){//check to see if the user wants this condition from menu
        System.out.println("Edited text: "+replaceExclamation(text));
      }
      if(mint.equals("s")){//check to see if the user wants this condition from menu
        System.out.println("Edited text: "+shortenSpace(text));
      }
   }while(!mint.equals("q"));//run untill q is pressed for 'quit'
}
  
  
  public static String sampleText(){//method to get a text
    String txt = "";
    do{Scanner in = new Scanner(System.in);
    System.out.println("Gimme a text");
    txt = in.nextLine();}while(txt.trim().length()<1);
    return txt.trim();
  } 
  public static String printMenu(){//method to print the menu page
    Scanner in = new Scanner(System.in);
    String chr;
    do{
    System.out.println("Menu\nc - number of non-whitespace characters\nw - number of words\nf - find text");
    System.out.println("r - replace all !'s\ns - shorten spaces\nq - quit\n\nChoose an option:");
    chr = in.nextLine().substring(0,1);
    if(!(chr.equals("c")||chr.equals("w")||chr.equals("f")||chr.equals("r")||chr.equals("s")||chr.equals("q")))
    System.out.println("Enter a valid choice.");}while(!(chr.equals("c")||chr.equals("w")||chr.equals("f")||chr.equals("r")||chr.equals("s")||chr.equals("q")));
    return chr;//when something that is not a letter offered is entered, say error
  }
  public static int getNumOfNonWSCharacters(String input){//method for number of letters in text
    input = input.replaceAll(" ","");//count number characters 
    return input.length();
  }
  public static int getNumOfWords(String input){//method for number of words
    input = input.trim();
    int num =1;
    for(int i =0;i<input.length()-1;i++){//counts number of words
      if(input.charAt(i) == ' '&&input.charAt(i+1)!=' '){
        num++;
      }
    }
    return num;
  }
  public static int findText(String look, String input){//method to find a specific combo in the text
    int num =0;
    while(input.indexOf(look) != -1){//look for entered patterns
      int at = input.indexOf(look);
      num++;
      input = input.substring(at + look.length(), input.length());
    }
    return num;
  }
  public static String replaceExclamation(String input){//method to replace exclamation point 
    input = input.replaceAll("!",".");
    return input;
  }
  public static String shortenSpace(String input){//method to limit number of spaces between words
    input = input.trim();
    input = input.replaceAll(" +"," ");
    return input;
  }
}