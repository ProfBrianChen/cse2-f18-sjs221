//Samantha Sagi
//9.6.18
//Check
//spliting the check with friends after dinner
import java.util.Scanner;
public class Check{
    			// main method required for every Java program
   			public static void main(String args[]) {
          Scanner myScanner = new Scanner( System.in );

System.out.print("Enter the original cost of the check in the form xx.xx: ");//how much the bill cost
double checkCost = myScanner.nextDouble();//enter in a decimal number
System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );//how good was the service
double tipPercent = myScanner.nextDouble();//enter in the value for tip percentage
tipPercent /= 100; //We want to convert the percentage into a decimal value
System.out.print("Enter the number of people who went out to dinner: ");//who was at the dinner
int numPeople = myScanner.nextInt();//enter in number of people there
double totalCost;//total cost has decimals 
double costPerPerson;//cost per person with decimals 
int dollars, dimes, pennies; //counts the amount each person needs to pay
totalCost = checkCost * (1 + tipPercent);//total cost with tip
costPerPerson = totalCost / numPeople;//total cost for each person with tip
dollars=(int)costPerPerson;//changed the cost per person into the dollors 
// (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
//  where the % (mod) operator returns the remainder
//  after the division:   583%100 -> 83, 27%5 -> 2 
dimes=(int)(costPerPerson * 10) % 10;//dimes people owe
pennies=(int)(costPerPerson * 100) % 10;//pennies people owe
System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);//final print of how much people owe 


}  //end of main method   
  	} //end of class
