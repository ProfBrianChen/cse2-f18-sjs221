//Samantha Sagi
//CSE-02-310
//Lab 06- Distplay Pyramids
import java.util.Scanner;
public class PatternD{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Please enter number of rows");//ask for number of rows
    int numbRows= myScanner.nextInt();
    while(numbRows<1 || numbRows>10){//sets range for number of rows
      System.out.println("invalid number, please enter a new value");
      numbRows= myScanner.nextInt();
    }
    for(int i=1; i<=numbRows;i++){//sets number of rows
      for(int j=(numbRows+1)-i;j>=1;--j){//sets pattern
       System.out.print(j+"     ");
      }
       System.out.println(" ");
       
        }
    }
  }