//Samantha Sagi
//CSE-02-310
//Lab 06- Distplay Pyramids
import java.util.Scanner;
public class PatternC{
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    System.out.println("Please enter number of rows");//ask for number of rows
    int numbRows= myScanner.nextInt();
    while(numbRows<1 || numbRows>10){
      System.out.println("invalid number, please enter a new value");//gives range for number of rows
      numbRows= myScanner.nextInt();
    }
     for(int i=1; i<=numbRows;i++){//sets number of rows
			 for(int j=numbRows;j>i;j--){//sets number of spaces to push the values over
          System.out.print(" ");
        }
      for(int j=i;j>=1;j--){//prints values
        System.out.print(j);
  }
      System.out.println(" ");
}
  }
}
