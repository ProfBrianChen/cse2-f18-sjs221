//Samantha Sagi
//9.4.18
//WelcomeClass
public class WelcomeClass{
  public static void main(String args[]){
    //prints WelcomeClass out
    System.out.println("      ------------");
    System.out.println("    | W E L C O M E |  ");
     System.out.println("      ------------");
     System.out.println("   ^   ^   ^   ^   ^   ^");
     System.out.println("  / \\ / \\ / \\ / \\ / \\ / \\ ");
     System.out.println(" <-S   J   S   2   2   1->");
     System.out.println("  \\ / \\ / \\ / \\ / \\ / \\ /");
     System.out.println("   v   v   v   v   v   v");
  }
}