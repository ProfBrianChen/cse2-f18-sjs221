//Samantha Sagi
//9.11.18
//Arithmetic
//calculate the cost and sales tax of different products
public class Arithmetic{
  public static void main(String args[]){
     //Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltPrice = 33.99;
 //the tax rate 
 double paSalesTax = 0.06;
double pantsCost = numPants * pantsPrice;//cost of pants without tax
double shirtsCost = numShirts * shirtPrice;//cost of shirts without tax
double beltCost = numBelts * beltPrice;//cost of belts without tax
double salesTaxPants = pantsCost * paSalesTax;//pants sales tax with hella decimals
salesTaxPants = salesTaxPants * 100;//sales tax multiplied by 100
double realsalesTaxPants = (int)salesTaxPants / 100;//real sales tax with two decimals 
double salesTaxShirt = shirtsCost * paSalesTax;//shirts sales tax with hella decimals 
salesTaxShirt = salesTaxShirt * 100;//sales tax multiplied by 100
double realsalesTaxShirt = (int)salesTaxShirt / 100;//real sales tax with two decimals
double salesTaxBelt = beltCost * paSalesTax;//sales tax with hella decimals 
salesTaxBelt = salesTaxBelt * 100;//sales tax multiplied by 100
double realsalesTaxBelt = (int)salesTaxBelt / 100;//real sales tax with two decimals 
double totalCostBeforeTax = pantsCost + shirtsCost + beltCost;//total cost of all clothing before tax
double totalSalesTax = realsalesTaxPants + realsalesTaxShirt + realsalesTaxBelt;//total sales tax of trasaction
double totalCostOfTransaction = totalSalesTax + totalCostBeforeTax;//total cost of trasaction 
System.out.println("total cost of pants is $" + pantsCost);
System.out.println("total cost of shirts is $" + shirtsCost);
System.out.println("total cost of belts is $" + beltCost);
System.out.println("sales tax of pants is $" + realsalesTaxPants);
System.out.println("sales tax of shirts is $" + realsalesTaxShirt);
System.out.println("sales tax of belts is $" + realsalesTaxBelt);
System.out.println("the total cost without sales tax is $" + totalCostBeforeTax);
System.out.println("the total sales tax is $" + totalSalesTax);
System.out.println("the total cost of the transaction is $" + totalCostOfTransaction);
  }
}


 
 

