import java.util.Random;
public class Arrays{
  public static void main(String [] args){
      int [] array1;
      array1 = new int[100];//set up the array
      int [] array2;
      array2 = new int[100];//set uop second array
      
      for (int i=0; i<100; i++){//for loop for first array 
        int randomInt = (int)(Math.random()*100);//generates random number 
        array1[i] = randomInt;//puts random number in array
      }
      
      for (int i = 0;i<100;i++){//or loop for second array
        for(int j = 0;j<100;j++){
        if (array1[j] == i){
          array2[i]++;//counts for the number of timesa a number pops up
          }
        }
      }
      
      for ( int i=0; i<100; i++){//for loop for print
        if(array2[i]==1)
        System.out.println(i +" occurs "+ array2[i]+" time");//print number of times the number occures
        else
          System.out.println(i + " occurs "+array2[i]+" times");
      }
      
  }
}
