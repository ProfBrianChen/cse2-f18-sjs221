//Samantha Sagi
//9.6.18
//Cyclometer
//measure the speed per sec of the bike rider
public class Cyclometer{
  public static void main(String args[]){
 double secsTrip1=480; //how long trip 1 lasted for
 double secsTrip2=3220; //how long trip 2 lasted for
 double countsTrip1=1561; //how many rotations of front wheel for trip 1
 double countsTrip2=9037; //how many rotation of front wheel for trip 2 
    
 double wheelDiameter=27.0; //how big the wheel diameter is
  double PI=3.14159; //value of pi
  double	feetPerMile=5280;  //number of feet per mile
  double	inchesPerFoot=12;   //number of inches per foot
  double	secondsPerMinute=60;  //number of seconds for minute
	double distanceTrip1, distanceTrip2,totalDistance;  //how long in miles the trips were
    
   System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
	 System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");

  //run the calculations; store the values. Document your
		//calculation here. calculate the trip by taking the number of sec and dividing by sec per minute
		//
		//
	distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//number of rotations times diameter times lenght
	totalDistance=distanceTrip1+distanceTrip2;//total distance

   //Print out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
 
 
  }
}