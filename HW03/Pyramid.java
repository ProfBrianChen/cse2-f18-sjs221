//Samantha Sagi
//9.11.18
//Pyramid
// prompts the user for the dimensions of a pyramid and returns the volume inside the pyramid
import java.util.Scanner;
public class Pyramid{
  public static void main(String args[]){
    Scanner myScanner = new Scanner( System.in );
     System.out.print("The square side of the pyramid is: ");//enter the side of the square
double squareSide = myScanner.nextDouble();
     System.out.print("The height of the pyramid is: ");//enter the height of the pyramid
double heightOfPyramid= myScanner.nextDouble();
double pyramidVolume = ((squareSide * squareSide) * heightOfPyramid)/3;//calculations of volume of the pyramid
    System.out.print("The volume inside the pyramid is: " + pyramidVolume);//print answer
  }
}
