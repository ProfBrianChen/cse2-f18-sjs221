//Samantha Sagi
//9.11.18
//Convert
// represent the number of acres of land affected by hurricane precipitation and how many inches of rain were dropped on average
import java.util.Scanner;
public class Convert{
  public static void main(String args[]){
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Enter the number of acres of land affected by hurricane precipitation: ");//enter number for acres affected 
double acresAffected = myScanner.nextDouble();
    System.out.print("Enter how inches of rain were dropped on average: ");//enter number for average rain
double rainDropAverage = myScanner.nextDouble();
double milesAffected = acresAffected / 640;//acres converted to miles
double rainMiles = rainDropAverage / 63360;//inches convered to miles
double cubicMiles = milesAffected * rainMiles;//calculations for cubic miles
    System.out.println(cubicMiles + "cubic miles");
    
  }
}
    